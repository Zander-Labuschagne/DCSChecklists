#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

echo -e "${CYAN}Compiling L-39C Checklist...${NC}"
cd L-39C\ Albatros
latexmk -silent --xelatex L-39C.tex
latexmk -silent -c --xelatex L-39C.tex
cp L-39C.pdf ~/Garage/Tresors/DCS/Docs/L-39\ Albatros/L-39C\ Checklist.pdf

echo -e "${CYAN}Compiling L-39ZA Checklist...${NC}"
cd ../L-39ZA\ Albatros
latexmk -silent --xelatex L-39ZA.tex
latexmk -silent -c --xelatex L-39ZA.tex
cp L-39ZA.pdf ~/Garage/Tresors/DCS/Docs/L-39\ Albatros/L-39ZA\ Checklist.pdf

echo -e "${CYAN}Compiling MiG-15bis Checklist...${NC}"
cd ../MiG-15bis\ Fagot
latexmk -silent --xelatex MiG-15bis.tex
latexmk -silent -c --xelatex MiG-15bis.tex
# TODO: Check if directory exists first, if not skip cp step
cp MiG-15bis.pdf ~/Garage/Tresors/DCS/Docs/MiG-15bis\ Fagot/MiG-15bis\ Checklist.pdf

echo -e "${CYAN}Compiling MiG-19P Checklist...${NC}"
cd ../MiG-19P\ Farmer\ B
latexmk -silent --xelatex MiG-19P.tex
latexmk -silent -c --xelatex MiG-19P.tex
cp MiG-19P.pdf ~/Garage/Tresors/DCS/Docs/MiG-19P\ Farmer\ B/MiG-19P\ Checklist.pdf

echo -e "${CYAN}Compiling MiG-21bis Checklist...${NC}"
cd ../MiG-21bis\ Fishbed
latexmk -silent --xelatex MiG-21bis.tex
latexmk -silent -c --xelatex MiG-21bis.tex
cp MiG-21bis.pdf ~/Garage/Tresors/DCS/Docs/MiG-21bis\ Fishbed/MiG-21bis\ Checklist.pdf

echo -e "${CYAN}Compiling F1CE Checklist...${NC}"
cd ../Mirage\ F1CE
latexmk -silent --xelatex F1CE.tex
latexmk -silent -c --xelatex F1CE.tex
cp F1CE.pdf ~/Garage/Tresors/DCS/Docs/Mirage\ F1CE/F1CE\ Checklist.pdf


echo -e "${GREEN}Checklists compilations complete!${NC}"
